import java.awt.geom.Point2D

import HockeyImplicits._

abstract class BaseSubStrategy extends SubStrategy{
  def pushOpponents(implicit self: ExtHockeyist,
                    world: ExtWorld,
                    game: ExtGame,
                    extMove: ExtMove,
                    mainStrategy: MyStrategy,
                    puck: ExtPuck,
                    selfPoint: Point2D): Boolean = {
    val visibleOpponentUnits = world.opponentTeamOnFieldWithoutGoalie.filter(self.visibleArea.contains(_))
    if (self.isActive && !visibleOpponentUnits.isEmpty) {
      extMove.moveToPoint(visibleOpponentUnits.head)
      extMove.strikeToUnit(visibleOpponentUnits.head)
      true
    }
    false
  }
}
