import java.awt.geom.{Point2D, Rectangle2D}

import EField._

class EField(game: ExtGame) extends Rectangle2D.Double(game.rinkLeft, game.rinkTop, game.worldWidth,
  game.worldHeight) {

  val leftSide = new Rectangle2D.Double(game.rinkLeft, game.rinkTop, game.worldWidth / 2, game.worldHeight)
  val rightSide = new Rectangle2D.Double(game.rinkLeft + game.worldWidth / 2, game.rinkTop,
    game.worldWidth / 2, game.worldHeight)
  val myFieldHalf: Rectangle2D.Double = if (game.mySide == EFieldSide.LEFT) leftSide else rightSide
  val opponentsFieldHalf: Rectangle2D.Double = if (game.mySide == EFieldSide.LEFT) rightSide else leftSide
  val center: Point2D = PUCK_START_POINT
  def points(by:Int): Seq[Point2D] = for (
    x <- game.rinkLeft.toInt to game.rinkRight.toInt by by;
    y <- game.rinkTop.toInt to game.rinkBottom.toInt by by
  ) yield new Point2D.Double(x, y)

}

object EField {

  val PUCK_START_POINT = new Point2D.Double(512, 446.5)
}
