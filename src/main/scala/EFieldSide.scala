object EFieldSide extends Enumeration {
  type Side = Value
  val LEFT, RIGHT = Value
}