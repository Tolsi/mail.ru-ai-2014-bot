import java.awt.geom.{Arc2D, Point2D, Rectangle2D, RectangularShape}

import model.Player

class EGates(val player: Player, val game: ExtGame, val fieldSide: EFieldSide.Side)
  extends GExtLine2D(player.netFront, player.netTop + ExtGame.PUCK_RADIUS*1.05, player.netFront,
    player.netBottom - ExtGame.PUCK_RADIUS*1.05) {

  import EGates._

  type StrikeArea = Set[RectangularShape]

  // Первая точка - верхняя штанга
  // Вторая точка - нижняя штанга

  val strikeAreas: Set[StrikeArea] = if (fieldSide == EFieldSide.LEFT)
    Set(
      // левая половина верхняя точка
      Set(
        new Arc2D.Double(getP1.getX - STRIKE_RADIUS, getP1.getY - STRIKE_RADIUS, STRIKE_RADIUS * 2, STRIKE_RADIUS * 2, -ANGLE_START, ANGLE_EXTENT, Arc2D.PIE)
        , new Rectangle2D.Double(200, 620, STRIKE_RECT_WIDTH, STRIKE_RECT_HEIGHT)
      ),
      // левая половина нижняя точка
      Set(
        new Arc2D.Double(getP2.getX - STRIKE_RADIUS, getP2.getY - STRIKE_RADIUS, STRIKE_RADIUS * 2, STRIKE_RADIUS * 2, ANGLE_START, -ANGLE_EXTENT, Arc2D.PIE)
        , new Rectangle2D.Double(200, 150, STRIKE_RECT_WIDTH, STRIKE_RECT_HEIGHT)
      )
    )
  else
    Set(
      // правая половина верхняя точка
      Set(
        new Arc2D.Double(getP1.getX - STRIKE_RADIUS, getP1.getY - STRIKE_RADIUS, STRIKE_RADIUS * 2, STRIKE_RADIUS * 2, ANGLE_START - 180, -ANGLE_EXTENT, Arc2D.PIE),
        new Rectangle2D.Double(360, 620, STRIKE_RECT_WIDTH, STRIKE_RECT_HEIGHT)
      ),
//      // правая половина нижняя точка
      Set(
        new Arc2D.Double(getP2.getX - STRIKE_RADIUS, getP2.getY - STRIKE_RADIUS, STRIKE_RADIUS * 2, STRIKE_RADIUS * 2, -ANGLE_START + 180, ANGLE_EXTENT, Arc2D.PIE),
        new Rectangle2D.Double(360, 150, STRIKE_RECT_WIDTH, STRIKE_RECT_HEIGHT)
      )
    )

  //  val strikePoints: Array[Point2D] = if (fieldSide == EFieldSide.LEFT) Array(
  //    new Point2D.Double(400, 400),
  //    new Point2D.Double(400, 660))
  //  else Array(
  //    new Point2D.Double(770, 400),
  //    new Point2D.Double(770, 660)
  //  )
  lazy val strikePoints: Seq[Point2D] = game.field.points(50).filter(p => isInStrikeArea(p))

  def isInStrikeAreaToWhatCorner(implicit self: Point2D): Option[(EFieldSide.Side, EGatesCorner.Corner)] = {
    if (isInStrikeArea) {
      strikeAreas.toIndexedSeq.indexOf(whatStrikeArea.get) match {
        case 0 => Some(fieldSide, EGatesCorner.TOP)
        case 1 => Some(fieldSide, EGatesCorner.BOTTOM)
      }
    } else None
  }

  def isInStrikeArea(implicit self: Point2D): Boolean = {
    whatStrikeArea.isDefined
  }

  def isYIn(y:Double): Boolean = getY1 <= y && getY2 >= y

  def whatStrikeArea(implicit self: Point2D): Option[StrikeArea] = {
    strikeAreas.filter(_.forall(_.contains(self))).headOption
  }

  def getFarCornerPointFromPoint(point: Point2D): EGatesCorner.Corner = {
    val p1Distance = point.distance(getP1)
    val p2Distance = point.distance(getP2)
    if (p1Distance > p2Distance) EGatesCorner.TOP else EGatesCorner.BOTTOM
  }

  def getFarGatesPointFromPoint(point: Point2D): Point2D = {
    val p1Distance = point.distance(getP1)
    val p2Distance = point.distance(getP2)
    if (p1Distance > p2Distance) getP1 else getP2
  }
}

object EGates {
  val STRIKE_RADIUS = 550
  val ANGLE_START = 35
  val ANGLE_EXTENT = -25

  val STRIKE_RECT_WIDTH = 640
  val STRIKE_RECT_HEIGHT = 190
}