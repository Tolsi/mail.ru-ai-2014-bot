import java.awt.geom.Point2D

import model.{Game, World}
import HockeyImplicits._

class ExtGame(game: Game, world: World) extends Game(game.randomSeed, game.tickCount, game.worldWidth,
  game.worldHeight, game.goalNetTop, game.goalNetWidth, game.goalNetHeight, game.rinkTop,
  game.rinkLeft, game.rinkBottom, game.rinkRight, game.afterGoalStateTickCount, game.overtimeTickCount,
  game.defaultActionCooldownTicks, game.swingActionCooldownTicks, game.cancelStrikeActionCooldownTicks,
  game.actionCooldownTicksAfterLosingPuck, game.stickLength, game.stickSector, game.passSector,
  game.hockeyistAttributeBaseValue, game.minActionChance, game.maxActionChance, game.strikeAngleDeviation,
  game.passAngleDeviation, game.pickUpPuckBaseChance, game.takePuckAwayBaseChance, game.maxEffectiveSwingTicks,
  game.strikePowerBaseFactor, game.strikePowerGrowthFactor, game.strikePuckBaseChance, game.knockdownChanceFactor,
  game.knockdownTicksFactor, game.maxSpeedToAllowSubstitute, game.substitutionAreaHeight, game.passPowerFactor,
  game.hockeyistMaxStamina, game.activeHockeyistStaminaGrowthPerTick, game.restingHockeyistStaminaGrowthPerTick,
  game.zeroStaminaHockeyistEffectivenessFactor, game.speedUpStaminaCostFactor, game.turnStaminaCostFactor,
  game.takePuckStaminaCost, game.swingStaminaCost, game.strikeStaminaBaseCost, game.strikeStaminaCostGrowthFactor,
  game.cancelStrikeStaminaCost, game.passStaminaCost, game.goalieMaxSpeed, game.hockeyistMaxSpeed,
  game.struckHockeyistInitialSpeedFactor, game.hockeyistSpeedUpFactor, game.hockeyistSpeedDownFactor,
  game.hockeyistTurnAngleFactor, game.versatileHockeyistStrength, game.versatileHockeyistEndurance,
  game.versatileHockeyistDexterity, game.versatileHockeyistAgility, game.forwardHockeyistStrength,
  game.forwardHockeyistEndurance, game.forwardHockeyistDexterity, game.forwardHockeyistAgility,
  game.defencemanHockeyistStrength, game.defencemanHockeyistEndurance, game.defencemanHockeyistDexterity,
  game.defencemanHockeyistAgility, game.minRandomHockeyistParameter, game.maxRandomHockeyistParameter,
  game.struckPuckInitialSpeedFactor, game.puckBindingRange) {


  val me = world.myPlayer.get
  val opponent = world.opponentPlayer.get

  var field: EField = new EField(this)

  var myGoalieX: Double = if (me.netFront == 65) me.netFront + ExtGame.HOCKEYIST_RADIUS else me.netFront - ExtGame.HOCKEYIST_RADIUS

  var opponentGoalieX: Double = if (opponent.netFront == 65) opponent.netFront + ExtGame.HOCKEYIST_RADIUS else opponent.netFront - ExtGame.HOCKEYIST_RADIUS

  var mySide: EFieldSide.Side = if (me.netFront == 65) EFieldSide.LEFT else EFieldSide.RIGHT

  var opponentSide: EFieldSide.Side = if (opponent.netFront == 65) EFieldSide.LEFT else EFieldSide.RIGHT

  var myGates: EGates = new EGates(me, this, mySide)

  var opponentGates: EGates = new EGates(opponent, this, opponentSide)

  def isFarXToGates(point: Point2D, gates: EGates, farDistance: Int): Boolean = distanceXToGates(point, gates) < farDistance

  def distanceXToGates(point: Point2D, gates: EGates): Double = {
    Math.max(gates.getX1, point.getX)-Math.min(gates.getX1, point.getX)
  }

  def isPuckHitsTheGatesAfterStrike(puck: ExtPuck, striker: ExtHockeyist): Boolean = {
    val puckVelocity = puck.strikeVelocityModule(0.75d + 0.25d * (striker.swingTicks / game.maxEffectiveSwingTicks), striker.speed.length, striker.angle, striker.angleTo(striker.nextPos()))
    val puckVectorAfterStrike = new GVector2D(puck.center, puckVelocity).rotate(striker.angle)
    return puckVectorAfterStrike.collideByY(puck, opponentGates).isDefined
  }

  def isOpponentsGoalieNotHaveTimeAfterStrike(puck: ExtPuck, goalie: ExtHockeyist, ticks: Int)(implicit self: ExtHockeyist, game: ExtGame): (Boolean, Option[Int], Option[Point2D]) = {
    val puckVelocity = puck.strikeVelocityModule(0.75d + 0.25d * (self.swingTicks / game.maxEffectiveSwingTicks), self.speed.length, self.angle, self.angleTo(self.nextPos()))
    val puckVectorAfterStrike = new GVector2D(puck.center, puckVelocity).rotate(self.angle)
    var goalieCenterAfterTicks = goalie.center
    for (i <- 1 to ticks) {
      val puckCenterAfterTicks = puckVectorAfterStrike.scaleTo(i + 1).toLine(puck).getP2

      // todo вектор движения шайбы с отражениями?
      if (!game.field.contains(puckCenterAfterTicks)) {
        return (false, None, None)
      }

      val puckAndGoalieYDifference = puckCenterAfterTicks.getY - goalieCenterAfterTicks.getY match {
        case diff if diff < -6 => -6
        case diff if diff > 6 => 6
        case diff => diff
      }
      goalieCenterAfterTicks = new Point2D.Double(goalieCenterAfterTicks.getX, goalieCenterAfterTicks.getY + puckAndGoalieYDifference)
      val goalieShape = if (game.mySide == Left) new GHockeyistShape(goalieCenterAfterTicks).leftHalf else new GPuckShape(goalieCenterAfterTicks).rightHalf
      val extraPuckShape = new GPuckShape(puckCenterAfterTicks, ExtPuck.RADIUS*2)
      if (goalieShape.intersects(extraPuckShape.getBounds)) return (false, None, None)
      //      val fromPuckToGoalieRad = angleBetweenPoints(goalieCenterAfterTicks,puckCenterAfterTicks)
      //      val fromPuckToGoalieDegree = Math.toDegrees(fromPuckToGoalieRad)
      if (game.opponentGates.ptLineDist(puckCenterAfterTicks) < ExtHockeyist.RADIUS) {
        return (true, Some(i), Some(puckCenterAfterTicks))
      }
    }
    return (false, None, None)
  }
}

object ExtGame {
  val HOCKEYIST_RADIUS = 30

  val GOALIE_RADIUS = 30

  val PUCK_RADIUS = 20

  val HOCKEYIST_WEIGHT = 30

  val GOALIE_WEIGHT = Integer.MAX_VALUE

  val PUCK_WEIGHT = 5
}
