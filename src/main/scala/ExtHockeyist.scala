import java.awt.geom.{Arc2D, Point2D}
import java.lang.Math.PI

import HockeyImplicits._
import model.{Hockeyist, HockeyistType}

class ExtHockeyist(hockeyist: Hockeyist) extends Hockeyist(hockeyist.id, hockeyist.playerId, hockeyist.teammateIndex,
  hockeyist.mass, hockeyist.radius, hockeyist.x, hockeyist.y, hockeyist.speedX, hockeyist.speedY,
  hockeyist.angle, hockeyist.angularSpeed, hockeyist.teammate, hockeyist.hokeyistType, hockeyist.strength,
  hockeyist.endurance, hockeyist.dexterity, hockeyist.agility, hockeyist.stamina, hockeyist.state,
  hockeyist.originalPositionIndex, hockeyist.remainingKnockdownTicks, hockeyist.remainingCooldownTicks,
  hockeyist.swingTicks, hockeyist.lastAction, hockeyist.lastActionTick) {

  import ExtHockeyist._

  lazy val isKnockdown: Boolean = remainingKnockdownTicks > 0
  lazy val isCooldown: Boolean = remainingCooldownTicks > 0
  lazy val isActive: Boolean = !(isKnockdown || isCooldown)
  lazy val visibleArea = visibleArc(hockeyist)
  lazy val speed = implicitly[GVector2D](this)
  lazy val angleVector = new GVector2D(this, 1).rotate(angle) // todo -?

  def farWithPuckToEnemies(implicit world:ExtWorld, puck:ExtPuck) = world.opponentTeamOnFieldWithoutGoalie.forall(h => {
    var nextArcs: Seq[Arc2D] = for (i <- 1 to 2) yield ExtHockeyist.visibleArc(h.nextPos(), if (h.angleTo(puck) > ExtHockeyist.ACCESS_ANGLE) ExtHockeyist.ACCESS_ANGLE else h.angleTo(puck))
    nextArcs = nextArcs :+ h.visibleArea
    !(h.lastActionTick.isDefined && h.lastActionTick.get > 0) || nextArcs.zipWithIndex.forall { case (vis, index) => !(vis.contains(puck.nextPos(index)) || vis.contains(nextPos(index)))}
  })

  def change(x: Double, y: Double, speedX: Double, speedY: Double,
            angle: Double, angularSpeed: Double): ExtHockeyist = {
    new ExtHockeyist(new Hockeyist(id, playerId, teammateIndex,
      mass, radius, x, y, speedX, speedY,
      angle, angularSpeed, teammate, hokeyistType, strength,
      endurance, dexterity, agility, stamina, state,
      originalPositionIndex, remainingKnockdownTicks, remainingCooldownTicks,
      swingTicks, lastAction, lastActionTick))
  }

  def nextPos(ticks: Int = 1): Point2D = if (ticks == 0) this else if (ticks > 1) speed.scaleTo(ticks).toLine(this).getP2 else speed.toLine(this).getP2

  def strikeVelocityModule(strikePower: Double,
                           strikerSpeed: Double,
                           strikerAngle: Double,
                           strikerSpeedAngle: Double): Double = {
    4.0 * strikePower +
      strikerSpeed * Math.cos(strikerAngle - strikerSpeedAngle)
  }

  lazy val isGoalie: Boolean = hokeyistType == HockeyistType.Goalie

  def angleTo(point2D: Point2D): Double = angleTo(point2D.getX, point2D.getY)

  def distanceTo(point2D: Point2D): Double = {
    distanceTo(point2D.getX, point2D.getY)
  }

  def getClosestsByDistancePoint(points: Seq[Point2D]): Point2D = {
    points.minBy(this.distance)
  }

  def getClosestsByAnglePoint(points: Seq[Point2D]): Point2D = {
    points.minBy(this.angleTo)
  }

  def getClosestsByDistanceAndAnglePoint(points: Seq[Point2D]): Point2D = {
    points.minBy(p=>3*this.angleTo(p) + this.distance(p))
  }
}

object ExtHockeyist {

  val ACCESS_DISTANCE = 120

  val ACCESS_ANGLE = PI / 12d

  val DISTANCE_TO_PUCK = 55

  val RADIUS = 30

  def visibleArc(hockeyist: model.Unit): Arc2D = visibleArc(hockeyist, hockeyist.angle)

  def visibleArc(center: Point2D, angle: Double): Arc2D = new Arc2D.Double(center.getX - ACCESS_DISTANCE, center.getY - ACCESS_DISTANCE, ACCESS_DISTANCE * 2,
    ACCESS_DISTANCE * 2, Math.toDegrees((-angle - ACCESS_ANGLE)), Math.toDegrees(ACCESS_ANGLE * 2), Arc2D.PIE)
}
