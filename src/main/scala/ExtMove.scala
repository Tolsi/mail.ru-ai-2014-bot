import java.awt.geom.Point2D
import java.lang.Math._

import HockeyImplicits._
import model.{ActionType, Move}

import scala.collection.mutable

class ExtMove(move: Move, world: ExtWorld) {

  import ExtMove._

  implicit val maxSpeed: (ExtHockeyist, Point2D) => Double = (h, p) => 1d
  // todo остановиться на точке
  implicit val stopAtPointSpeed: (ExtHockeyist, Point2D) => Double = (h, p) => {
    (h distance p, h angleTo p) match {
      // тормозим
      case (d, a) if d < 30 && abs(a) < START_SPEEDUP_ANGLE => -h.norm
      case (d, a) if abs(a) < START_SPEEDUP_ANGLE => 1d
      case (d, a) if abs(a-PI) < START_SPEEDUP_ANGLE => -1d
      case _ => 0d
    }
  }

  implicit val maxSmartSpeed: (ExtHockeyist, Point2D) => Double = (h, p) => {
    (h distance p, h angleTo p) match {
      case (d, a) if abs(a) < START_SPEEDUP_ANGLE => 1d
      case _ => 0d
    }
  }

  def rotateToPuck()(implicit self: ExtHockeyist, puck: ExtPuck): Unit = {
    cancelSwinging()
    rotateToPoint(puck)
    takePuck()
  }

  def takePuck()(implicit self: ExtHockeyist, puck: ExtPuck): Unit = {
    if (self.visibleArea.contains(puck)) {
      move.action = ActionType.TakePuck
    }
  }

  // todo для всего? для хокеиста для пинания. и не только поворот
  def moveToPuck(maxNextTicks: Int = 5)(implicit self: ExtHockeyist, puck: ExtPuck, game: ExtGame): Unit = {
    cancelSwinging()
    val nextByDistancePuckPosition = puck.nextPos(min((self.distanceTo(puck) / self.speed.length).toInt, maxNextTicks))
    val moveTo: Point2D = if (game.field.contains(nextByDistancePuckPosition)) nextByDistancePuckPosition else puck
    //    smartMoveToPoint(moveTo)
    simpleMoveToPointWithSpeed(moveTo, stopAtPointSpeed)
    takePuck()
  }

  def smartTakePuck()(implicit self: ExtHockeyist, puck: ExtPuck): Unit = {
    cancelSwinging()
    // todo если другие могут взять, то уступить и забрать позже?
    if (self.visibleArea.contains(puck)) {
      val opponentPlayerViewPuck = !world.opponentTeamWithoutGoalie.filter(h => h.visibleArea.contains(puck)).isEmpty
      if (opponentPlayerViewPuck && !waitToTakePuckTick.contains(self.teammateIndex)) {
        waitToTakePuckTick.put(self.teammateIndex, world.tick + 1)
      }
      waitToTakePuckTick.get(self.teammateIndex).orElse(Some(world.tick)).map(
        waitTick => if (waitTick <= world.tick) {
          move.action = ActionType.TakePuck
          waitToTakePuckTick.remove(self.teammateIndex)
        }
      )
    }
  }

  def simpleMoveToPointWithSpeed(point: Point2D, speedUp: (ExtHockeyist, Point2D) => Double)(implicit self: ExtHockeyist, puck: ExtPuck): Unit = {
    cancelSwinging()
    val angleToPoint = self.angleTo(point)
    move.turn = angleToPoint
    move.speedUp = speedUp(self, point)
  }

  def moveToPoint(point: Point2D, inPointDistance: Double = ExtHockeyist.RADIUS * 0.8, maxBackMoveDistance: Double = BACK_SMART_MOVE_DISTANCE)(implicit self: ExtHockeyist, puck: ExtPuck): Boolean = {
    // todo если через несколько тиков на точке, то уже не двигаться?
    if (self.distanceTo(point) < inPointDistance || (for (i <- 1 to 2) yield self.nextPos(i).distance(point)).exists(_< inPointDistance)) {
      false
    } else {
      smartMoveToPoint(point, maxBackMoveDistance)
      true
    }
  }

  private def smartMoveToPoint(point: Point2D, maxBackMoveDistance: Double = BACK_SMART_MOVE_DISTANCE)(implicit self: ExtHockeyist, puck: ExtPuck): Unit = {
    cancelSwinging()
    val angleToPoint = self.angleTo(point)
    val distanceToPoint = self.distance(point)
    var updatedPoint: Point2D = null
    if (angleToPoint > PI / 2 && distanceToPoint < maxBackMoveDistance) {
      move.turn = angleToPoint - PI
      updatedPoint = new Point2D.Double(point.getX - BACK_SMART_MOVE_POINT_SHIFT, point.getY + BACK_SMART_MOVE_POINT_SHIFT)
    } else if (angleToPoint < -PI / 2 && distanceToPoint < maxBackMoveDistance) {
      move.turn = angleToPoint + PI
      updatedPoint = new Point2D.Double(point.getX - BACK_SMART_MOVE_POINT_SHIFT, point.getY + BACK_SMART_MOVE_POINT_SHIFT)
    } else {
      move.turn = angleToPoint
      updatedPoint = point
    }
    move.speedUp = maxSmartWithBackSpeed(self, updatedPoint, maxBackMoveDistance)
  }

  def maxSmartWithBackSpeed(h: ExtHockeyist, p: Point2D, maxBackMoveDistance: Double = BACK_SMART_MOVE_DISTANCE): Double = {
    (h distance p, h angleTo p) match {
      case (d, a) if d < maxBackMoveDistance && (a < -PI / 2 || a > PI / 2) && abs(if (a < 0) a + PI else a - PI) < START_SPEEDUP_ANGLE => -1 * (d / BACK_SMART_STOP_DISTANCE)
      case (d, a) if abs(a) < START_SPEEDUP_ANGLE => 1d * (d / BACK_SMART_STOP_DISTANCE)
      case _ => 0
    }
  }

  def cancelSwinging()(implicit self: ExtHockeyist): Unit = {
    if (self.swingTicks > 0) {
      move.action = ActionType.CancelStrike
    }
  }

  // todo толкаться
  def strikeToUnit(hockeyist: ExtHockeyist)(implicit self: ExtHockeyist, game: ExtGame, puck: ExtPuck): Unit = {
    // todo можем ли размахнуться?
    val angleToPoint = self.angleTo(hockeyist)
    if (abs(angleToPoint) < VISIBLE_ANGLE) {
      // todo точнее расчёт размаха
      if (self.swingTicks < game.maxEffectiveSwingTicks && !(game.myGates.isInStrikeArea(hockeyist)) && hockeyist.swingTicks == 0 && self.angleTo(hockeyist) < VISIBLE_ANGLE * 0.9 && /* todo три! */ (for (i<-1 to 3) yield ExtHockeyist.visibleArc(self.nextPos(i), self.angle).contains(hockeyist.nextPos(i))).forall(_==true)) {
        move.action = ActionType.Swing
      } else {
        move.action = ActionType.Strike
      }
    } else {
      rotateToPoint(hockeyist)
    }
  }

  def strikeToPuck()(implicit self: ExtHockeyist, game: ExtGame, puck: ExtPuck): Unit = {
    // todo можем ли размахнуться?
    val angleToPoint = self.angleTo(puck)
    if (abs(angleToPoint) < VISIBLE_ANGLE) {
      if (self.swingTicks < game.maxEffectiveSwingTicks && (self.angleTo(puck) < VISIBLE_ANGLE / 3 && ExtHockeyist.visibleArc(self.nextPos(), self.angle).contains(puck.nextPos()) && self.speed.length >= puck.speed.length)) {
        move.action = ActionType.Swing
      } else {
        move.action = ActionType.Strike
      }
    } else {
      rotateToPoint(puck)
    }
  }

  def strikeToGatesPoint(point: Point2D, prodictNextTicks:Int = 2)(implicit self: ExtHockeyist, game: ExtGame, world: ExtWorld, puck: ExtPuck): Boolean = {
    // todo можем ли размахнуться?
    val angleToPoint = self.angleTo(point)
    val strikeToGatesCondition = (abs(angleToPoint) < STRIKE_ANGLE || self.swingTicks > 0) && game.isPuckHitsTheGatesAfterStrike(puck, self)
    if (strikeToGatesCondition) {
      val swingCondition = if (world.opponentGoalie.isDefined) {
        val ifNextTicksInGoodPos = self.speed.length < 2 || (for (i <- 1 to prodictNextTicks) yield self.nextPos(i)).forall(p => game.opponentGates.isInStrikeArea(p) && /* для учёта поворота условие попадание по-другому!*/ new GVector2D(p, point).collideByY(p, game.opponentGates).isDefined)
        self.swingTicks < game.maxEffectiveSwingTicks && self.farWithPuckToEnemies && ifNextTicksInGoodPos && /* вкл размах :) */ game.isOpponentsGoalieNotHaveTimeAfterStrike(puck, world.opponentGoalie.get, 100)._1
        // todo размах
        // todo расчитывать поворот для замаха
      } else {
        val ifNextTicksInGoodPos = self.speed.length < 2 || (for (i <- 1 to prodictNextTicks) yield self.nextPos(i)).forall(p => game.opponentGates.isInStrikeArea(p) && new GVector2D(p, point).collideByY(p, game.opponentGates).isDefined)
        self.swingTicks < game.maxEffectiveSwingTicks && self.farWithPuckToEnemies && ifNextTicksInGoodPos
      }
      if (swingCondition) {
        move.action = ActionType.Swing
      } else {
        move.action = ActionType.Strike
      }
      true
    }
    else false
  }

  def passToPoint(point: Point2D)(implicit self: ExtHockeyist): Unit = {
    cancelSwinging()
    move.passPower = 1
    val angleToPoint = self.angleTo(point)
    if (abs(angleToPoint) <= PASS_ANGLE) {
      move.passAngle = angleToPoint
      move.action = ActionType.Pass
    } else {
      rotateToPoint(point)
    }
  }

  def rotateToPoint(point: Point2D)(implicit self: ExtHockeyist): Unit = {
    cancelSwinging()
    val angleToPoint = self.angleTo(point)
    move.turn = angleToPoint
  }
}

object ExtMove {
  val START_SPEEDUP_ANGLE = 40
  val STRIKE_ANGLE = 1.0D * PI / 180.0D
  val PASS_ANGLE = 1.0D * PI / 3
  val VISIBLE_ANGLE = 1.0D * PI / 3
  val BACK_SMART_MOVE_DISTANCE = 150
  val BACK_SMART_STOP_DISTANCE = 30
  val BACK_SMART_MOVE_POINT_SHIFT = 7

  // для длительных действий
  val hockeyistLastMoveStartTick = mutable.Map[Int, Int]()
  val waitToTakePuckTick = mutable.Map[Int, Int]()
}
