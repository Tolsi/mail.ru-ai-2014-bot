import java.awt.geom.{Line2D, Point2D}

import HockeyImplicits._
import model.{Hockeyist, Puck}

class ExtPuck(puck: Puck, private var world: ExtWorld) extends Puck(puck.id, puck.mass, puck.radius,
  puck.x, puck.y, puck.speedX, puck.speedY, puck.ownerHockeyistId, puck.ownerPlayerId) {

  lazy val center = implicitly[Point2D](this)

  lazy val shape = new GPuckShape(this)

  lazy val speed = implicitly[GVector2D](this)
  lazy val isMine = ownerPlayerId.isDefined && ownerPlayerId.get == world.myPlayer.get.id

  def passVelocityModule(passPower: Double,
                         strikerSpeed: Double,
                         strikerAngle: Double,
                         strikerSpeedAngle: Double,
                         passAngle: Double): Double = {
    15.0 * passPower +
      strikerSpeed *
        Math.cos(strikerAngle + passAngle - strikerSpeedAngle)
  }

  def strikeVelocityModule(strikePower: Double,
                           strikerSpeed: Double,
                           strikerAngle: Double,
                           strikerSpeedAngle: Double): Double = {
    20.0 * strikePower +
      strikerSpeed * Math.cos(strikerAngle - strikerSpeedAngle)
  }

  def isControl(hockeyist: Hockeyist): Boolean = ownerPlayerId.isDefined && ownerHockeyistId.get == hockeyist.id

  def getNextTicksPosition(ticksCount: Int): GVector2D = speed.scaleTo(ticksCount)

  def getAngleTo(point2D: Point2D): Double = getAngleTo(point2D)

  def isIntersectsThisLine(line: Line2D): Boolean = {
    // напрямую
    speed.collideByY(this, line).isDefined
  }

  lazy val takeChance = if (world.hockeyistWithPuckId.isEmpty) 160 - 110 * (speed.length / 22) else 25
  lazy val strikeChance = if (world.hockeyistWithPuckId.isEmpty) 175 - 110 * (speed.length / 22) else 75

  def nextPos(ticks:Int = 1):Point2D = if (ticks>1) speed.scaleTo(ticks).toLine(this).getP2 else speed.toLine(this).getP2

  def isIntersectsThisLine(line: Line2D, ticks: Int): Boolean = {
    val intersectionPoint = speed.collideByY(this, line)
    intersectionPoint.isDefined && distanceTo(intersectionPoint.get) <= speed.norm * ticks
  }

  def distanceTo(point2D: Point2D): Double = {
    distanceTo(point2D.getX, point2D.getY)
  }
}

object ExtPuck {
  val RADIUS = 20
}