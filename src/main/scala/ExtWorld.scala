import model.{HockeyistType, Hockeyist, World}

class ExtWorld(world: World) extends World(world.tick, world.tickCount, world.width, world.height,
  world.players, world.hockeyists, world.puck) {

  import ExtWorld._

  lazy val myGoalieSpeedY = myGoalie.map(_.y - myGoalieLastY.getOrElse(0d))
  lazy val opponentGoalieSpeedY = opponentGoalie.map(_.y - opponentGoalieLastY.getOrElse(0d))

  myGoalieLastY = myGoalie.map(_.y)
  opponentGoalieLastY = opponentGoalie.map(_.y)

  lazy val myTeamWithoutGoalie = filterHockeyist(h => h.teammate && !h.isGoalie)
  lazy val myTeamOnFieldWithoutGoalie = filterHockeyist(h => h.teammate && !h.isGoalie && h.originalPositionIndex > -1)
  lazy val myTeamOnField = filterHockeyist(h => h.teammate && h.originalPositionIndex > -1)
  lazy val opponentTeamWithoutGoalie = filterHockeyist(h => !h.teammate && !h.isGoalie)
  lazy val opponentTeamOnFieldWithoutGoalie = filterHockeyist(h => !h.teammate && !h.isGoalie && h.originalPositionIndex > -1)
  lazy val opponentTeamOnField = filterHockeyist(h => !h.teammate && h.originalPositionIndex > -1)
  lazy val opponentGoalie = filterHockeyist(h => !h.teammate && h.hokeyistType == HockeyistType.Goalie).headOption
  lazy val opponentHasNotGoalie = opponentGoalie.isEmpty
  lazy val IHasNotGoalie = myGoalie.isEmpty
  lazy val myGoalie = filterHockeyist(h => h.teammate && h.hokeyistType == HockeyistType.Goalie).headOption
  lazy val hockeyistWithPuckId = puck.ownerHockeyistId
  lazy val hockeyistWithPuck = filterHockeyist(h => hockeyistWithPuckId.isDefined && h.id == hockeyistWithPuckId.get).headOption
  override val puck = new ExtPuck(world.puck, this)

  def myTeamOnFieldWithoutMe(me: Hockeyist) = myTeamOnField.filterNot(_.id == me.id)

  def myTeamOnFieldWithoutGoalieAndMe(implicit me: Hockeyist) = myTeamOnField.filterNot(h => h.id == me.id || h.hokeyistType == HockeyistType.Goalie)

  def filterHockeyist(predicate: ExtHockeyist => Boolean): Seq[ExtHockeyist] = {
    hockeyists.map(h => new ExtHockeyist(h)).filter(predicate)
  }
}

object ExtWorld {
  var myGoalieLastY: Option[Double] = None
  var opponentGoalieLastY: Option[Double] = None
}