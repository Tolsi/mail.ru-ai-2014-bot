import java.awt.geom.{Arc2D, Ellipse2D, Point2D}

class GCircle(var radius: Double, val center: Point2D) extends Ellipse2D.Double(center.getX - radius,
  center.getY - radius, radius * 2, radius * 2) {
  lazy val leftHalf = new Arc2D.Double(getX, getY, getWidth, getHeight, 90, 180, Arc2D.PIE)
  lazy val rightHalf = new Arc2D.Double(getX, getY, getWidth, getHeight, -90, 180, Arc2D.PIE)
  lazy val topHalf = new Arc2D.Double(getX, getY, getWidth, getHeight, -180, 180, Arc2D.PIE)
  lazy val bottomHalf = new Arc2D.Double(getX, getY, getWidth, getHeight, 0, 180, Arc2D.PIE)
}