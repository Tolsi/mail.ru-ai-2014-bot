import java.awt.geom.{Line2D, Point2D}

class GExtLine2D(p1: Point2D, p2: Point2D) extends Line2D.Double(p1, p2) {

  lazy val center = new Point2D.Double((getX1 + getX2) / 2, (getY1 + getY2) / 2)

  def this(x1: Double,
           y1: Double,
           x2: Double,
           y2: Double) {
    this(new Point2D.Double(x1, y1), new Point2D.Double(x2, y2))
  }
}
