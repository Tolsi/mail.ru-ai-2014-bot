import java.awt.geom.Point2D

class GPuckShape(center:Point2D, radius:Double = ExtPuck.RADIUS) extends GCircle(radius, center)
