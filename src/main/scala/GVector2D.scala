import java.awt.geom.{AffineTransform, Line2D, Point2D}

class GVector2D(val x: Double = 0, val y: Double = 0) extends Cloneable {

  import GVector2D._

  lazy val norm: Double = Math.sqrt(x * x + y * y)
  lazy val length = Math.abs(norm)
  lazy val angle: Double = Math.atan2(y, x)
  lazy val normalize: GVector2D = {
    if (norm == 0) {
      new GVector2D()
    } else {
      val d = norm
      new GVector2D(x / d, y / d)
    }
  }
  lazy val normal: GVector2D = {
    if (norm == 0) new GVector2D()
    else {
      val alpha = Math.atan2(y, x)
      new GVector2D(Math.sin(alpha), -Math.cos(alpha))
    }
  }
  lazy val inverse: GVector2D = new GVector2D(-x, -y)

  def scalarProduct(v: GVector2D): Double = (x * v.x + y * v.y)

  def minus(v: GVector2D): GVector2D = new GVector2D(x - v.x, y - v.y)

  def rotate(alpha: Double): GVector2D = {
    val xs = x * Math.cos(alpha) - y * Math.sin(alpha)
    val ys = x * Math.sin(alpha) + y * Math.cos(alpha)
    new GVector2D(xs, ys)
  }

  def add(v: GVector2D): GVector2D = new GVector2D(x + v.x, y + v.y)

  def toLine(start: Point2D): Line2D = new Line2D.Double(start, new Point2D.Double(start.getX + x, start.getY + y))

  def scaleTo(d: Double): GVector2D = new GVector2D(x * d, y * d)

  def translate(p: Point2D): Point2D = {
    new Point2D.Double(p.getX + x, p.getY + y)
  }

  def isColinear(p0: Point2D, v1: GVector2D, p1: Point2D): Boolean = {
    isColinear(p0, v1, p1, ASSUME_ZERO, ASSUME_ANGLE_ZERO)
  }

  def isColinear(p0: Point2D,
                 v1: GVector2D,
                 p1: Point2D,
                 delta: Double,
                 delta_angle: Double): Boolean = {
    val d = angle - v1.angle
    if ((Math.abs(d) > delta_angle) && (Math.abs(d - Math.PI) > delta_angle) &&
      (Math.abs(d + Math.PI) > delta_angle)) return false
    if (normalProjection(p0, p1).distance(p1) > delta) return false
    true
  }

  def normalProjection(o: Point2D, p: Point2D): Point2D = {
    if (norm == 0) return p
    val t = ((p.getX - o.getX) * x + (p.getY - o.getY) * y) / (x * x + y * y)
    val px = t * x + o.getX
    val py = t * y + o.getY
    new Point2D.Double(px, py)
  }

  override def clone(): AnyRef = {
    new GVector2D(this)
  }

  def this(v: GVector2D) {
    this(v.x, v.y)
  }


  def this(p0: Point2D, p1: Point2D) {
    this(p1.getX - p0.getX, p1.getY - p0.getY)
  }

  def this(line: Line2D) {
    this(line.getP1, line.getP2)
  }

  def this(p: Point2D, length: Double) {
    this(p, new Point2D.Double(p.getX + length, p.getY))
  }

  def transform(tr: AffineTransform): GVector2D = {
    val xdst = x * tr.getScaleX + y * tr.getShearX
    val ydst = x * tr.getShearY + y * tr.getScaleY
    new GVector2D(xdst, ydst)
  }

  def intersection(vectorStart: Point2D, l: Line2D): Option[Point2D] = Option(intersectionPoint(vectorStart, new GVector2D(l.getP1, l.getP2), l.getP1))


  def intersectionPoint(p0: Point2D, v1: GVector2D, p1: Point2D): Point2D = {
    if ((Math.abs(x) > ASSUME_ZERO) && (Math.abs(v1.x) > ASSUME_ZERO)) {
      val a0 = y / x
      val b0 = p0.getY - a0 * p0.getX
      val a1 = v1.y / v1.x
      val b1 = p1.getY - a1 * p1.getX
      if (Math.abs(a0 - a1) < ASSUME_ZERO) return null
      val px = (b0 - b1) / (a1 - a0)
      val py = a0 * px + b0
      new Point2D.Double(px, py)
    } else if ((Math.abs(x) < ASSUME_ZERO) && (Math.abs(v1.x) > ASSUME_ZERO)) {
      val px = p0.getX
      val a1 = v1.y / v1.x
      val b1 = p1.getY - a1 * p1.getX
      val py = a1 * px + b1
      new Point2D.Double(px, py)
    } else if ((Math.abs(x) > ASSUME_ZERO) && (Math.abs(v1.x) < ASSUME_ZERO)) {
      val px = p1.getX
      val a0 = y / x
      val b0 = p0.getY - a0 * p0.getX
      val py = a0 * px + b0
      new Point2D.Double(px, py)
    } else null
  }

  def collideByY(vectorStart: Point2D, l: Line2D): Option[Point2D] = {
    val intersectPoint = Option(intersectionPoint(vectorStart, new GVector2D(l.getP1, l.getP2), l.getP1))
    if (intersectPoint.isDefined) {
      if (l.getY1 <= intersectPoint.get.getY && intersectPoint.get.getY <= l.getY2)
        intersectPoint
      else
        None
    } else None
  }
}

object GVector2D {

  val ASSUME_ZERO = 0.0001d

  val ASSUME_ANGLE_ZERO = 0.017d

  def transform(p: Point2D, tr: AffineTransform): Point2D = {
    val x = p.getX
    val y = p.getY
    val dst = new Point2D.Double(x * tr.getScaleX + y * tr.getShearX + tr.getTranslateX, x * tr.getShearY + y * tr.getScaleY + tr.getTranslateY)
    dst
  }
}
