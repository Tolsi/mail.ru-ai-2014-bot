import java.awt.geom.Point2D

import model.Unit
import Math._

object HockeyImplicits {

  implicit def Unit2Point2D(unit: Unit) =
    new Point2D.Double(unit.x, unit.y)

  implicit def Unit2Vector(unit: Unit) =
    new GVector2D(unit, new Point2D.Double(unit.x + unit.speedX, unit.y + unit.speedY))


  implicit def ExtHockeyist2Shape(hockeyist: ExtHockeyist) =
    new GHockeyistShape(hockeyist)

  def angleBetweenPoints(p1: Point2D, p2: Point2D): Double = {
    val absoluteAngleTo: Double = atan2(p2.getY - p1.getY, p2.getX - p1.getX)
    absoluteAngleTo
  }
}