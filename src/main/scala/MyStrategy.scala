import java.awt.geom.Point2D

import model._

import scala.collection.mutable
import HockeyImplicits._

class MyStrategy extends Strategy {

  import MyStrategy._

  private var self: Hockeyist = _
  private var world: World = _
  private var game: Game = _

  override def move(self: Hockeyist,
                    world: World,
                    game: Game,
                    move: Move) {
    this.self = self
    this.world = world
    this.game = game

    if (world.tick != currentTick) {
      first()
      lastWorld = extWorld
      extWorld = new ExtWorld(world)
      currentTick = world.tick
    }
    val extMove = new ExtMove(move, extWorld)

    val justGoal = world.myPlayer.get.justMissedGoal || world.myPlayer.get.justScoredGoal
    if (justGoal) {
      hockeyistsStrategiesMap.clear()
      // todo замены
      return
    }

    val newStrategy = selectNewStrategy()
    val isStrategyNew = setNewStrategy(newStrategy)
    if (isStrategyNew) println("Новая стратегия!")

    // todo чувак с шайбой - сразу нападающий?
    // запуск стратегии хокеиста
    val extHockeyist = new ExtHockeyist(self)
    println(s"id: ${extHockeyist.id} speed: ${extHockeyist.speed.length} point: ${implicitly[Point2D](extHockeyist)}")
    val hockeyistStrategy = hockeyistsStrategiesMap.get(self.teammateIndex)
    hockeyistStrategy.getOrElse({
      // todo стратегии в зависимости от положения в игре - счёта, владельца и положения шайбы
      val newStrategy = selectNewStrategy()
      setNewStrategy(newStrategy)
      newStrategy
    }).move(extHockeyist, extWorld,
        extGame, extMove, this)

  }

  def setNewStrategy(strategy: SubStrategy): Boolean = {
    val newStrategyClass = strategy.getClass
    val currentStrategy = hockeyistsStrategiesMap.get(self.teammateIndex)
    val isSameType = currentStrategy.isDefined && currentStrategy.get.getClass.equals(newStrategyClass)
    if (!isSameType) {
      hockeyistsStrategiesMap += self.teammateIndex -> strategy
    }
    !isSameType
  }

  def selectNewStrategy(): SubStrategy = {
    val puck = extWorld.puck
    val mates = extWorld.myTeamOnFieldWithoutGoalieAndMe(self)
    val atackers = hockeyistsStrategiesMap.values.count { case h: SAtackSubStrategy => true; case _ => false}
    if (extWorld.puck.isMine) {
      if (extWorld.puck.isControl(self)
        // и кто-то защищает ворота или на подходе :)
        && mates.exists(m => /*extGame.distanceXToGates(m, extGame.myGates) <= 500 && */ hockeyistsStrategiesMap.get(m.teammateIndex).map { case _: SDefenceSubStrategy => true; case _ => false}.getOrElse(false))
      ) {
        new SAtackSubStrategy
      } else {
        new SDefenceSubStrategy
      }
      //extGame.field.myFieldHalf.contains(puck) &&
    } else {
      // и кто-то защищает ворота
      // todo если он вблизи дальней штанги
      val opponentsStayOnOnePlace = extWorld.opponentTeamOnFieldWithoutGoalie.forall(h=>h.speed.length < 0.5 && h.angularSpeed < Math.PI / 10)
      val   withoutMeDefOnPlace = mates.exists(m => extGame.distanceXToGates(m, extGame.myGates) <= 130 && hockeyistsStrategiesMap.get(m.teammateIndex).map { case _: SDefenceSubStrategy => true; case _ => false}.getOrElse(false))
      val   anyDefOnPlace = extWorld.myTeamOnFieldWithoutGoalie.exists(m =>
        extGame.distanceXToGates(m, extGame.myGates) <= 130 && hockeyistsStrategiesMap.get(m.teammateIndex).map { case _: SDefenceSubStrategy => true; case _ => false}.getOrElse(false)
      )
      // и больше нет атакующих
      val noOtherAttackers = !mates.exists(m => hockeyistsStrategiesMap.get(m.teammateIndex).map { case _: SAtackSubStrategy => true; case _ => false}.getOrElse(false))
      if ( //extWorld.puck.ownerHockeyistId.isEmpty ||
        withoutMeDefOnPlace && (extWorld.hockeyists.minBy(_.distance(puck)).id == self.id // или я ближайший к шайбе
          && puck.takeChance >= 100) //и можно взять шайбу
          || (self.distance(puck) < 200 && extWorld.hockeyistWithPuck.map(!_.teammate).getOrElse(false)) // или она недалеко
//          && atackers <= 1
//          && noOtherAttackers
          || (extWorld.hockeyistWithPuck.isEmpty &&
            extGame.distanceXToGates(puck, extGame.myGates) < 300 &&
            extGame.distanceXToGates(self, extGame.myGates) < 150)
          || opponentsStayOnOnePlace
          // и если я не выигрываю
          //&& world.myPlayer.get.goalCount <= world.opponentPlayer.get.goalCount
      ) {
          new SAtackSubStrategy
      } else {
        // если нет защиты и на ударной позиции враг, то пытаемся убить
        if (extWorld.hockeyistWithPuck.isDefined &&
          !extWorld.hockeyistWithPuck.get.teammate &&
          extGame.distanceXToGates(self, extGame.myGates) > extGame.distanceXToGates(extWorld.hockeyistWithPuck.get, extGame.myGates) &&
          extGame.myGates.isInStrikeArea(extWorld.hockeyistWithPuck.get)
        ) {
          new SAtackSubStrategy
        } else {
          new SDefenceSubStrategy
        }
      }
    }
  }

  // при первом запуске стратегии
  def first() {
    if (firstRun) {
      extGame = new ExtGame(game, world)
      firstRun = false
    }
  }
}

object MyStrategy {

  var extWorld: ExtWorld = _
  var lastWorld: ExtWorld = _


  var currentTick: Int = -1

  var firstRun: Boolean = true

  var hockeyistsStrategiesMap = mutable.Map[Int, SubStrategy]()
  var extGame: ExtGame = _

  def removeStrategyForHockeyist(hockeyist: Hockeyist) {
    hockeyistsStrategiesMap -= hockeyist.teammateIndex
  }
}
