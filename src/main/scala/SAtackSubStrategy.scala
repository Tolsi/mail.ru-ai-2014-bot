import java.awt.geom.Point2D

import HockeyImplicits._
import model.ActionType

class SAtackSubStrategy extends BaseSubStrategy {

  var closestPointAtTick: Option[(Point2D, Int)] = None

  override def move(implicit self: ExtHockeyist,
                    world: ExtWorld,
                    game: ExtGame,
                    extMove: ExtMove,
                    mainStrategy: MyStrategy) {
    implicit val puck = world.puck
    implicit val selfPoint = implicitly[Point2D](self)

    if (puck.isMine) {
      if (world.puck.isControl(self)) {
        val opponentPlayer = world.opponentPlayer
        if (opponentPlayer.isDefined && !opponentPlayer.get.strategyCrashed) {
          var strikeTarget: Point2D = null
          val opponentGoalie = world.opponentGoalie
          if (opponentGoalie.isDefined) {
            val farPointFromGoalie = game.opponentGates.getFarGatesPointFromPoint(opponentGoalie.get)
            var strikeVector = new GVector2D(self, farPointFromGoalie)
            val grade = 1
            val rotateGrades = game.opponentGates.isInStrikeAreaToWhatCorner.map {
              case (EFieldSide.RIGHT, EGatesCorner.TOP) => grade
              case (EFieldSide.RIGHT, EGatesCorner.BOTTOM) => -grade
              case (EFieldSide.LEFT, EGatesCorner.BOTTOM) => grade
              case (EFieldSide.LEFT, EGatesCorner.TOP) => -grade
            }.getOrElse(0)
            game.opponentGates.isInStrikeAreaToWhatCorner.map(println _)
            //            println(implicitly[Point2D](self))
            strikeTarget = new Point2D.Double(farPointFromGoalie.getX, new GVector2D(self, farPointFromGoalie).rotate(Math.toRadians(rotateGrades)).toLine(self).getP2.getY)
          } else {
            strikeTarget = game.opponentGates.center
            if (!extMove.strikeToGatesPoint(strikeTarget)) {
              extMove.moveToPoint(strikeTarget)
            }
            return
          }

          //        TODO пасс
          //          val teammates = world.myTeamOnFieldWithoutMe(self)
          //          if (!teammates.isEmpty) {
          //            val teamMate = teammates.head
          //            if (game.isFarXToSide(teamMate, game.opponentGates, 300) &&
          //              self.distanceTo(game.opponentGates.center) + 200 >
          //                teamMate.distanceTo(game.opponentGates.center) &&
          //              (game.isFarXToSide(teamMate, game.opponentGates, 75) ||
          //                self.distanceTo(game.opponentGates.center) > 500)) {
          //              extMove.passToPoint(teamMate)
          //              return
          //            }
          //          }
          if ((for (i <- 0 to 3) yield game.opponentGates.isInStrikeArea(self.nextPos(i))).exists(_==true)) {
            if (!extMove.strikeToGatesPoint(strikeTarget)) {
              extMove.moveToPoint(strikeTarget)
            }
            //            }
            return
          } else {
            if (game.distanceXToGates(self, game.opponentGates) < 60) {
//              val teamMate = game.myGates.center
              extMove.passToPoint(game.myGates.center)
              return
            } else {
              var moveTarget: Point2D = null
              // todo выбор точки под угол?
              // todo выбор точки дальней от врагов
              if (closestPointAtTick.isDefined && (
                closestPointAtTick.get._2 < world.tick + 20)) {
                moveTarget = closestPointAtTick.get._1
              } else {
                // todo точка с минимальным (углом от неё до ворот) и (от тебя до ворот)
                moveTarget = self.getClosestsByDistancePoint(game.opponentGates.strikePoints)
                closestPointAtTick = Some(moveTarget, world.tick)
              }
              // если на точке, но не в регионе удара
              if (self.distanceTo(moveTarget) < 10) {
                // todo пасс?
                moveTarget = self.getClosestsByDistancePoint(game.opponentGates.strikePoints.diff(Seq(moveTarget)))
                closestPointAtTick = Some(moveTarget, world.tick)
              }
              extMove.moveToPoint(moveTarget)
            }
            return
          }
        } else {
          val target = self.getClosestsByDistancePoint(game.opponentGates.strikePoints)
          if (game.opponentGates.isInStrikeArea) {
            if (!extMove.strikeToGatesPoint(target)) {
              extMove.moveToPoint(target)
            }
          }
        }
      } else {
        //        val moveTarget = self.getClosestsPoint(game.opponentGates.strikePoints)
        extMove.moveToPoint(game.myGates.center)
      }
    } else {
      // todo агрессивно отбирать, отдельная стратегия?
      // только один
      val teammates = world.myTeamOnFieldWithoutMe(self)
      var mateLastStrikeTick = Int.MinValue
      if (!teammates.isEmpty) {
        val teamMate = teammates.head
        if (teamMate.lastAction == ActionType.Strike) mateLastStrikeTick = teamMate.lastActionTick.get
      }
      if (world.hockeyistWithPuck.isEmpty &&
        puck.isIntersectsThisLine(game.opponentGates, 50) ||
        mateLastStrikeTick + 10 > world.tick) {
        extMove.moveToPoint(game.myGates.center)
      } else {
        // todo сбивать с ног бьющего
        if (pushOpponents) return
        if (world.hockeyistWithPuck.isDefined)
          if (self.visibleArea.contains(world.hockeyistWithPuck.get)) {
            extMove.strikeToUnit(world.hockeyistWithPuck.get)
          } else {
            extMove.simpleMoveToPointWithSpeed(world.hockeyistWithPuck.get.nextPos(Math.min(3, (self.distanceTo(world.hockeyistWithPuck.get)/self.speed.length).toInt)), extMove.maxSmartSpeed)
          }
        else
          extMove.moveToPuck(1)
      }
    }
  }

}