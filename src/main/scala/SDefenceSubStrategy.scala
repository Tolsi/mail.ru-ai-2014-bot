import java.awt.geom.Point2D
import HockeyImplicits._

class SDefenceSubStrategy extends BaseSubStrategy {

  override def move(implicit self: ExtHockeyist,
                    world: ExtWorld,
                    game: ExtGame,
                    extMove: ExtMove,
                    mainStrategy: MyStrategy) {
    implicit val puck = world.puck
    implicit val selfPoint = implicitly[Point2D](self)

    if (!puck.isMine) {
      // todo быстрее выходить на позиции
      takeTheDefencePosition
    } else {
      if (puck.isControl(self)) {
//        extMove.passToPoint(world.myTeamOnFieldWithoutGoalieAndMe.head)
        val moveTarget = self.getClosestsByDistanceAndAnglePoint(game.opponentGates.strikePoints)
        extMove.rotateToPoint(moveTarget)
        return
      } else {
        takeTheDefencePosition
      }
    }
    if (self.visibleArea.contains(puck) && !puck.isMine) {
      // подстроиться по скорости и направлению
      if (puck.takeChance >= 80 && (for (i<-0 to 11) yield self.visibleArea.contains(puck.nextPos(i))).forall(_==true)) {
        extMove.takePuck()
      } else {
        // если шанс завладеть мал и летит в ворота, то отбиваем
        if (puck.isIntersectsThisLine(game.myGates)) {
          if (puck.strikeChance < 75) println(s"О НЕТ! ${puck.strikeChance}")
          extMove.strikeToPuck()
        } else {
          extMove.takePuck()
        }
      }
    } else if (game.distanceXToGates(puck, game.myGates) > 500 || game.distanceXToGates(self, game.myGates) > 130)
      // если шайба далеко или сам ещё не у ворот, то можем потолкаться
      pushOpponents
  }

  def takeTheDefencePosition(implicit self: ExtHockeyist,
                             world: ExtWorld,
                             game: ExtGame,
                             extMove: ExtMove,
                             mainStrategy: MyStrategy,
                             puck: ExtPuck,
                             selfPoint: Point2D) = {
//    val hWithPuck = world.hockeyistWithPuck
//    todo если вдруг будет усталость, то раскомментить if (hWithPuck.isDefined && !hWithPuck.get.teammate) {
      if (
        !self.visibleArea.contains(puck)
//        && !world.hockeyistWithPuck.map(h => !h.teammate && game.myGates.isInStrikeArea(h)).getOrElse(false)
        && (!game.myGates.isYIn(
        /*можно быть чуть дальше ворот по Y*/
        if (game.myGates.getFarCornerPointFromPoint(puck) == EGatesCorner.TOP)
            self.y + ExtHockeyist.RADIUS * 0.2
          else
            self.y  - ExtHockeyist.RADIUS * 0.2
        )
        || game.distanceXToGates(self, game.myGates) > ExtHockeyist.RADIUS * 3) ) {
        extMove.moveToPoint(if (world.myGoalie.isDefined) {
          new Point2D.Double(game.myGoalieX + (if (game.mySide == EFieldSide.LEFT) ExtHockeyist.RADIUS*1.4 else -ExtHockeyist.RADIUS*1.4),
            if (game.myGates.getFarCornerPointFromPoint(puck) == EGatesCorner.TOP)
              game.myGates.getFarGatesPointFromPoint(puck).getY + ExtHockeyist.RADIUS * 1.3
            else
              game.myGates.getFarGatesPointFromPoint(puck).getY - ExtHockeyist.RADIUS * 1.3)
        }
        else {
          new Point2D.Double(game.myGoalieX, game.myGates.center.getY)
        }, maxBackMoveDistance=350, inPointDistance = ExtHockeyist.RADIUS * 1.2)
      } else {
        extMove.rotateToPuck()
      }
//    } else if (!extMove.moveToPoint(
//      if (world.myGoalie.isDefined)
//        new Point2D.Double(game.myGoalieX + (
//          if (game.mySide == EFieldSide.LEFT) ExtHockeyist.RADIUS * 1.3 else -ExtHockeyist.RADIUS * 1.3), game.myGates.center.getY)
//      else new Point2D.Double(game.myGoalieX, game.myGates.center.getY),maxBackMoveDistance=550)) {
//      extMove.rotateToPuck()
//    }
  }

}
