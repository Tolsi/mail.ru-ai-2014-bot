object SHockeyistStrategies extends Enumeration {
  type Strategy = Value
  val GOALIE, DEFENDER, ATTACKER, REPLACE = Value
}