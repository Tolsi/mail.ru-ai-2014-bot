object STeamStrategies extends Enumeration {
  type Strategy = Value
  val DEFENCE, NORMAL, ATTACK = Value
}