

trait SubStrategy {

  def move(implicit self: ExtHockeyist,
           world: ExtWorld,
           game: ExtGame,
           move: ExtMove,
           mainStrategy: MyStrategy): Unit
}
